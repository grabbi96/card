import rootHtml from './template.js';
import cC from './colorChange';
import titleChange from './titleChnage'

let body = document.getElementsByTagName("BODY")[0];

body.onload = function(){
     body.innerHTML = rootHtml.app
     textSelect()  
     cC.colorChange(true)
     titleChange(true)
}
let paragraph = null;
let paraArea = null;
let stopBackgroundBtn = null;
function textSelect (){
    paragraph = document.querySelector('.card-text');
    paraArea = document.getElementById('para-area');
    stopBackgroundBtn = document.getElementById('stopbg');
    let title = document.querySelector('card-title');
    paragraph.onclick = textChange;
    stopBackgroundBtn.onclick = stopBackgroundFunc;
    
}

let newText = null;
function textChange(){
    let paraValue = paragraph.innerHTML;
    paragraph.parentNode.removeChild(paragraph);
    let textArea = document.createElement('textarea');
    textArea.id = "newtext"
    textArea.className = "form-control"
    textArea.setAttribute('style', 'height:210px; margin-bottom:10px')
    textArea.innerHTML = paraValue;
    paraArea.appendChild(textArea)
    newText = document.getElementById('newtext'); 
    newText.onblur = changeValue;
}
function changeValue(){
    let newValue = newText.value;
    paragraph.innerHTML = newValue;
    paraArea.appendChild(paragraph);
    paraArea.removeChild(newText);
}
let btnValue = true;
function stopBackgroundFunc(e){
    e.preventDefault();
    btnValue = !btnValue;
    if(!btnValue){
        stopBackgroundBtn.innerHTML = 'Start Again';
        cC.colorChange(btnValue);
    }else{
        stopBackgroundBtn.innerHTML = 'Stop  Background';
        
        cC.colorChange(btnValue);
    }
}




// paragraph.style.background = 'red';
// paragraph.onclick = function(e){
//     e.preventDefault();
//     console.log(23)
// }