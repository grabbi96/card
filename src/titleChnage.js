 let titleChange = function(val){
    let cartTitle = document.querySelector('.card-title');
    console.log(cartTitle)
    let arr = [cartTitle.innerHTML.trim(), 'New Title']

    let titleFunc = null
    let flag = true
    if(val){
        titleFunc = setInterval(function(){
            if(flag){
                cartTitle.innerHTML = arr[0]
                flag = false
            }else{
                cartTitle.innerHTML = arr[1]
                flag = true
            }
        }, 1000)
        
    }else{
        clearInterval(titleFunc)
    }
}
export default titleChange;