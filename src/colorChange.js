let fColor = 100;
let sColor = 100;
let tColor = 100;
let mColor = 1;
let colorSet;
let colorChange = (bool)=>{

    if(bool){
        colorSet = setInterval(function(){
            let cart = document.querySelector('.card');
            if(fColor >= 255){
                fColor = 0;
            }
            if(sColor >= 255){
                sColor = 0;
            }
            if(tColor >= 255){
                tColor = 0;
            }
            if(mColor >= 1){
                mColor = 0.1;
            }
            tColor += 10;
            sColor += 5;
            fColor += 4;
            mColor += .1;
        
            cart.style.background = 'rgba('+fColor+', '+sColor+', '+tColor+', '+mColor+')';
        }, 100)
    }else{
        clearInterval(colorSet)
    }
    
}
export default {
    colorChange,
};